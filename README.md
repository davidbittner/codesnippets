# Code Snippets

Below, listed are code snippets that I think best represent my skillset so far as a programmer. There is a slight description as to why I think this was a good snippet to include as well as a simple explanation as to what it does (just for ease of reading).

## CSV Translator

[Project GitLab Page](https://gitlab.com/davidbittner/translator)

[FunctionBranch.java](https://gitlab.com/davidbittner/codesnippets/-/blob/master/src/FunctionBranch.java#L83)

### Background
The CSV Translator is a piece of software used internally at Unanet written over a few summers during my internships.

It is responsible for transforming CSV data based on an intermediate script file.

This script file is in a custom language that is tokenized, lexed, and put into an AST (Abstract Syntax Tree) all within the same code-base. This code does not use any libraries for interpreting or for generating that syntax tree.

Functions can be called within this custom scripting language.
The way these functions are implemented, is through Java Reflection.

When a function is seen within this script file (during deserialization), it iterates over the available methods within 'Function Dictionaries'. These are simply classes that are flagged as containing methods that can be called within the scripting language.

### Description
The following snippet uses the Java reflection API to store handles to these aforementioned methods, allowing incredibly easy development of new functions that can be used by the CSV Translator.

For example, to add a new function, one simply just needs to write the Java code for how that function should work, and it is now immediately accessable within the scripting language.

Additionally, it supports variadic functions and multiple definitions of functions with varying parameter counts.

Full Snippet:
```java
/**
 * Sets the method based on the name and parameter count.
 */
private void setMethod()
{
    if(methodDictionary == null) {
        methodDictionary = new ArrayList<>();
        for(String dict : FUNC_DICTIONARIES) {
            try {
                Class<?> dict_class = Class.forName(dict);
                methodDictionary.addAll(Arrays.asList(dict_class.getMethods()));
            }catch(ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }

        //Removes non-public functions (functions that aren't all caps)
        for(int i = 0; i < methodDictionary.size(); i++) {
            String name = methodDictionary.get(i).getName();
            if(!name.toUpperCase().equals(name)) {
                methodDictionary.remove(i);
                i--;
            }
        }
    }

    ArrayList<Method> candidates = new ArrayList<>();
    for(Method method : methodDictionary) {
        if(method.getName().equals(me.text)) {
            candidates.add(method);
        }
    }

    if(candidates.size() == 0) {
        throw new TokenException(me, String.format("Function '%s' does not exist.", me.text));
    }else{
        candidates.sort(Comparator.comparing(Method::getParameterCount));
        if(candidates.get(0).getParameterCount() > this.children.size()) {
            throw new TokenException(me, getParameterError(candidates));
        }else{
            for(Method method : candidates) {
                if(method.getParameterCount() == this.children.size()) {
                    myFunction = method;
                    return;
                }
            }
            for(Method method : candidates) {
                if(method.getParameterCount() == 1 &&
                   method.getParameterTypes()[0] == String[].class) {
                    myFunction = method;
                    variadic = true;
                    return;
                }
            }

            throw new TokenException(me, getParameterError(candidates));
        }
    }
}
```

## HTTP-WebServer

[Project GitLab Page](https://gitlab.com/davidbittner/http-webserver/)

[webserver.rs](https://gitlab.com/davidbittner/codesnippets/-/blob/master/src/webserver.rs#L24)

### Background
For an assignment at Old Dominion University, I was tasked with writing an HTTP webserver from scratch in my language of choice.

I ended up choosing Rust, as it was a language I had been having a lot of fun writing in at the time, and because it has a lot of great networking primitives within it's standard library.

All HTTP parsing and basic HTTP auth is handwritten within this project as well.

### Description
This snippet waits and listens for incoming connections. It uses what I consider to be, a fairly simple but clean solution for dealing with multiple concurrent connections.

When a new connection is received, it spawns a new thread of execution and hands off the TcpSocket so data can be received and deserialized within another data structure.

Here in the WebServer though, there is a HashMap that links the incoming address with the associated thread of execution. When a connection is to be terminated, the main WebServer thread can use a multi-producer single-consumer (MPSC) channel to receive the address of which connection terminated. At this point, that thread can be removed from this HashMap to be cleanly joined. This allows any information on the execution of this connection (such as errors) to be aggregated and handled in one location.

It also allows an arbitrary number of concurrent connections so that HTTP Requests can be pipelined if desired.

Full Snippet:
```rust
impl WebServer {
    pub fn new() -> io::Result<Self> {
        info!("creating new webserver...");
        let addr = format!("{}:{}", CONFIG.addr, CONFIG.port);

        let listener = TcpListener::bind(&addr)?;
        info!("bound to addr '{}' successfully", addr);

        listener.set_nonblocking(true)?;
        Ok(WebServer { listener })
    }

    pub fn listen(&mut self) -> io::Result<()> {
        let mut conn_map = HashMap::new();
        let (t_tx, t_rx) = channel();

        loop {
            io::stdout().flush()?;
            match self.listener.accept() {
                Ok((stream, addr)) => {
                    trace!("new connection received: '{}'", addr);

                    let handler = SocketHandler::new(stream)?;

                    let other_tx = t_tx.clone();
                    let handle = std::thread::spawn(move || {
                        let res = handler.dispatch();
                        other_tx.send(addr).expect("failed to send addr");

                        return res;
                    });

                    conn_map.insert(addr, handle);
                }
                Err(err) => {
                    use io::ErrorKind;

                    match err.kind() {
                        ErrorKind::WouldBlock => (),
                        _ => error!(
                            "error occured while accepting connection: '{}'",
                            err
                        ),
                    }
                }
            }

            let del = t_rx.recv_timeout(Duration::from_millis(10));
            match del {
                Ok(addr) => {
                    let thread = conn_map.remove(&addr).expect(
                        "attempted to unwrap a connection that did not exist",
                    );

                    match thread.join() {
                        Err(err) => error!("a thread panicked: '{:?}'", err),
                        Ok(res) => match res {
                            Err(err) => error!(
                                "'{}' terminated with an error: '{}'",
                                addr, err
                            ),
                            Ok(_) => trace!("'{}' closed successfully", addr),
                        },
                    }
                }
                Err(_) => continue,
            }
        }
    }
}
```

## calc

[GitLab Project Page](https://gitlab.com/davidbittner/calc/)

[operator_exp.rs](https://gitlab.com/davidbittner/codesnippets/-/blob/master/src/operator_exp.rs#L92)

### Background
This is a project I've been working on in my free time for a command-line calculator. It is capable of parsing arbitrarily complex mathematical expressions into a syntax tree and allows custom functions to be implemented that can be called within the mathematical expressions.

### Description
The following snippet is taken from the parsing code. It is responsible for parsing any expression that involves an operator. For example, 2+2, 4*4, 3 * 2 + 4.

Full Snippet:
```rust
impl ParseExpr for OperatorExp {
    type Err = OperatorExpError;
    fn parse(s: &str, fd: &FuncDict) -> Result<Expression, Self::Err> {
        let mut s = s.trim();
        if let Some(loc) = find_matching(s, 0) {
            if loc == s.len()-1 {
                s = &s[1..s.len()-1];
            }
        }

        let mut oplocs: Vec<_> = OPERATOR_REG.find_iter(s)
            .map(|m| {
                let op_s = m.as_str();
                let op   = op_s.parse::<Operator>().unwrap();
                (
                    m.start(),
                    op,
                    count_depth(s, m.start()) * 10 + op.get_prec()
                )
            })
            .collect();

        //Sorts by the precedence of the operators
        oplocs.sort_by(|(_, _, a_p),(_, _, b_p)| {
                b_p.cmp(&a_p)
            });

        //Picks the operator that has the higheest precedence.
        let (st, op, _) = oplocs.pop()
            .ok_or(OperatorExpError::NoOperandError)?;

        //Finds the left and right side of this expression, and recursively parses those too.
        let a = Expression::parse(&s[0..st], fd)?;
        let b = Expression::parse(&s[st+1..], fd)?;

        Ok(Self {
            operand_a: a.into(),
            operand_b: b.into(),
            operator:  op
        }.into())
    }
}
```
