package unanet.translator.ast;

import unanet.translator.exceptions.ParsingException;
import unanet.translator.exceptions.TokenException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

public class FunctionBranch extends BaseBranch {
    static Logger logger = Logger.getGlobal();
    /**
     * The children of this function (the parameters)
     */
    List<BaseBranch> children;

    /**
     * A list of locations to search for functions used in the program
     */
    static final String[] FUNC_DICTIONARIES = {
            "unanet.translator.functions.StandardLibrary",
            "unanet.translator.functions.Variables",
            "unanet.translator.functions.GroupTotal",
            "unanet.translator.functions.LookupFuncs"
    };

    static ArrayList<Method> methodDictionary;

    /**
     * The function that gets invoked when this is proc'd
     */
    Method myFunction;

    /**
     * Whether or not the method is variadic
     */
    boolean variadic;

    /**
     * The token that contains line/character position information
     */
    Token me;

    /**
     * Generates a string that shows candidate functions
     * @param candidates The candidate functions to generate parameter lists from.
     * @return The format string.
     */
    private String getParameterError(ArrayList<Method> candidates) {
        StringBuilder errorMsg = new StringBuilder();

        errorMsg.append(String.format("The function '%s' with those parameters does not exist. Candidate(s) are:\n", me.text));
        for(Method method : candidates) {
            errorMsg.append(String.format("\t %s(", method.getName()));

            int i = 0;
            for(Parameter param : method.getParameters()) {
                if(param.getType() == String[].class) {
                    errorMsg.append(param.getName().toUpperCase() + "...");
                }else{
                    errorMsg.append(param.getName().toUpperCase() + "");
                }

                if(i != method.getParameterCount()-1) {
                    errorMsg.append(", ");
                }
                i++;
            }
            errorMsg.append(")\n");
        }

        return errorMsg.toString();
    }

    /**
     * Sets the method based on the name and parameter count.
     */
    private void setMethod()
    {
        if(methodDictionary == null) {
            methodDictionary = new ArrayList<>();
            for(String dict : FUNC_DICTIONARIES) {
                try {
                    Class<?> dict_class = Class.forName(dict);
                    methodDictionary.addAll(Arrays.asList(dict_class.getMethods()));
                }catch(ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            }

            //Removes non-public functions (functions that aren't all caps)
            for(int i = 0; i < methodDictionary.size(); i++) {
                String name = methodDictionary.get(i).getName();
                if(!name.toUpperCase().equals(name)) {
                    methodDictionary.remove(i);
                    i--;
                }
            }
        }

        ArrayList<Method> candidates = new ArrayList<>();
        for(Method method : methodDictionary) {
            if(method.getName().equals(me.text)) {
                candidates.add(method);
            }
        }

        if(candidates.size() == 0) {
            throw new TokenException(me, String.format("Function '%s' does not exist.", me.text));
        }else{
            candidates.sort(Comparator.comparing(Method::getParameterCount));
            if(candidates.get(0).getParameterCount() > this.children.size()) {
                throw new TokenException(me, getParameterError(candidates));
            }else{
                for(Method method : candidates) {
                    if(method.getParameterCount() == this.children.size()) {
                        myFunction = method;
                        return;
                    }
                }
                for(Method method : candidates) {
                    if(method.getParameterCount() == 1 &&
                       method.getParameterTypes()[0] == String[].class) {
                        myFunction = method;
                        variadic = true;
                        return;
                    }
                }

                throw new TokenException(me, getParameterError(candidates));
            }
        }
    }

    /**
     * A constructor that takes tokens and generates itself as well as children.
     * @param tokens The tokens used to generate it's children branches as well as itself with.
     */
    FunctionBranch(List<Token> tokens) {
        this.children = new ArrayList<>();
        this.me = tokens.get(0);

        try {
            this.children = ParserHelpers.parseParameters(tokens.subList(2, tokens.size()));
        } catch(ParsingException ex)
        {
            throw new TokenException(me, String.format("No matching parentheses found for function %s.", me.text));
        }

        try {
            setMethod();
        }catch(TokenException ex) {
            throw ex;
        }
    }

    /**
     * Processes this branch.
     * @return The return value of all of it's children as well as itself.
     */
    @Override
    public String proc() {
        String params[] = new String[children.size()];
        for(int i = 0; i < children.size(); i++)
        {
            params[i] = children.get(i).proc();
        }

        if(myFunction != null)
        {
            try {
                Object ret;
                if(variadic)
                {
                    ret = myFunction.invoke(null, new Object[] {params});
                }else {
                    ret = myFunction.invoke(null, (Object[])params);
                }

                if(ret == null) {
                    return null;
                }else{
                    return ret.toString();
                }
            } catch (InvocationTargetException ex) {
                throw new TokenException(
                        me,
                        String.format("Failed to invoke function: %s with the error:\n\t%s",
                                toString(),
                                ex.getTargetException().getMessage()));
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
                logger.severe("This shouldn't happen. Execution will continue.");
                return null;
            } catch (NullPointerException ex) {
                return null;
            }
        }else {
            logger.severe("Method pointer was null, this shouldn't happen.");
            System.exit(5);
        }
        return null;
    }

    /**
     * Serializes the itself as well as it's children to generate a string that should match the original template.
     * @return The string representation of itself and it's children.
     */
    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder(me.text + "(");
        for(int i = 0; i < children.size(); i++) {
            if(i < children.size()-1) {
                ret.append(children.get(i).toString());
                ret.append(", ");
            }else{
                ret.append(children.get(i).toString());
            }
        }
        ret.append(")");

        return ret.toString();
    }
}
